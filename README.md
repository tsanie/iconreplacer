# README #

Just replace your Destiny Child's avatar from this.

### How to use it? ###

* Double click it
* Drag the folder "asset/icon" to the list view
* Select one you want to change its avatar
* Change the index in the right panel which you want to replace the avatar to
* Or you can drag the custom picture to the preview box and replace with it
* Click the save button (it will **DIRECTLY** change the files, so make sure you had backuped them)

### Contact me ###

* [me@tsanie.us](mailto:me@tsanie.us)
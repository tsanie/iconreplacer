﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IconReplacer
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.App;
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowser.ShowDialog(this) == DialogResult.OK)
            {
                textIconFolder.Text = folderBrowser.SelectedPath;
                ParseFolder(folderBrowser.SelectedPath);
            }
        }

        private void numericIndex_ValueChanged(object sender, EventArgs e)
        {
            LoadPictures((int)numericIndex.Value);
        }

        private void listIcons_SelectedIndexChanged(object sender, EventArgs e)
        {
            var enabled = listIcons.SelectedIndices.Count > 0;
            panelRight.Enabled = enabled;
            if (enabled)
            {
                int index = listIcons.SelectedIndices[0];
                if (numericIndex.Value == index)
                {
                    LoadPictures(index);
                }
                else
                {
                    numericIndex.Value = index;
                }
            }
            else
            {
                panelIcon.BackgroundImage = null;
                panelBattle.BackgroundImage = null;
                panelIcon.Tag = null;
                panelBattle.Tag = null;
                buttonApply.Enabled = false;
            }
        }

        void LoadPictures(int index)
        {
            var id = (ID)listIcons.Items[index].Tag;
            panelIcon.BackgroundImage = id.Entries[id.Index].Image;
            panelBattle.BackgroundImage = id.EntriesBattle[id.Index].Image;
            panelIcon.Tag = null;
            panelBattle.Tag = null;
            buttonApply.Enabled = false;
        }

        private void textIconFolder_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var folder = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                e.Effect = Directory.Exists(folder) ? DragDropEffects.Copy : DragDropEffects.None;
            }
        }

        private void textIconFolder_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var folder = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                textIconFolder.Text = folder;
                ParseFolder(folder);
            }
        }

        private void panelIcon_DragEnter(object sender, DragEventArgs e)
        {
            DetectIfCanDrop(e);
        }

        private void panelIcon_DragDrop(object sender, DragEventArgs e)
        {
            DropBitmap(panelIcon, new Size(128, 128), e);
        }

        private void panelBattle_DragEnter(object sender, DragEventArgs e)
        {
            DetectIfCanDrop(e);
        }

        private void panelBattle_DragDrop(object sender, DragEventArgs e)
        {
            DropBitmap(panelBattle, new Size(186, 512), e);
        }

        private void DetectIfCanDrop(DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var file = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                e.Effect = File.Exists(file) && file.EndsWith(".png", StringComparison.OrdinalIgnoreCase) ? DragDropEffects.Copy : DragDropEffects.None;
            }
            else if (e.Data.GetDataPresent(DataFormats.Bitmap))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void DropBitmap(Panel panel, Size size, DragEventArgs e)
        {
            Bitmap image;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var file = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                try
                {
                    image = new Bitmap(Image.FromFile(file), size);
                }
                catch
                {
                    return;
                }
            }
            else if (e.Data.GetDataPresent(DataFormats.Bitmap))
            {
                image = new Bitmap((Image)e.Data.GetData(DataFormats.Bitmap), size);
            }
            else
            {
                image = null;
            }

            if (image != null)
            {
                panel.BackgroundImage = image;
                buttonApply.Enabled = true;
                panel.Tag = true;
            }
        }

        private void toolStripMenuItemLarge_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem menu in contextMenu.Items)
            {
                menu.CheckState = menu == toolStripMenuItemLarge ? CheckState.Checked : CheckState.Unchecked;
            }
            listIcons.View = View.LargeIcon;
        }

        private void toolStripMenuItemList_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem menu in contextMenu.Items)
            {
                menu.CheckState = menu == toolStripMenuItemList ? CheckState.Checked : CheckState.Unchecked;
            }
            listIcons.View = View.List;
        }

        private void toolStripMenuItemTile_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem menu in contextMenu.Items)
            {
                menu.CheckState = menu == toolStripMenuItemTile ? CheckState.Checked : CheckState.Unchecked;
            }
            listIcons.View = View.Tile;
        }

        private void buttonReplace_Click(object sender, EventArgs e)
        {
            if (listIcons.SelectedIndices.Count <= 0)
            {
                return;
            }
            var index = listIcons.SelectedIndices[0];
            var indexTo = (int)numericIndex.Value;
            if (indexTo < 0 || indexTo >= listIcons.Items.Count)
            {
                return;
            }
            var id = (ID)listIcons.Items[index].Tag;
            var idTo = (ID)listIcons.Items[indexTo].Tag;
            var from = id.Entries[id.Index];
            var fromBattle = id.EntriesBattle[id.Index];
            var to = idTo.Entries[idTo.Index];
            var toBattle = idTo.EntriesBattle[idTo.Index];

            from.Data = to.Data;
            from.Image = to.Image;
            iconList.Images[index] = to.Image;
            fromBattle.Data = toBattle.Data;
            fromBattle.Image = toBattle.Image;

            listIcons.Invalidate();
            LoadPictures(index);
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            if (listIcons.SelectedIndices.Count <= 0)
            {
                return;
            }
            var index = listIcons.SelectedIndices[0];
            var id = (ID)listIcons.Items[index].Tag;
            if (panelIcon.Tag != null)
            {
                var from = id.Entries[id.Index];
                using (var ms = new MemoryStream())
                {
                    panelIcon.BackgroundImage.Save(ms, ImageFormat.Png);
                    from.Data = ms.ToArray();
                }
                from.Image = panelIcon.BackgroundImage;
                iconList.Images[index] = from.Image;
                listIcons.Invalidate();
            }
            if (panelBattle.Tag != null)
            {
                var fromBattle = id.EntriesBattle[id.Index];
                using (var ms = new MemoryStream())
                {
                    panelBattle.BackgroundImage.Save(ms, ImageFormat.Png);
                    fromBattle.Data = ms.ToArray();
                }
                fromBattle.Image = panelBattle.BackgroundImage;
            }
            LoadPictures(index);
        }

        private void ParseFolder(string folder)
        {
            panelContainer.Enabled = false;
            ThreadPool.QueueUserWorkItem(DoParseFolder, folder);
        }

        void DoParseFolder(object obj)
        {
            var folder = (string)obj;
            var parser = new Parser
            {
                Folder = folder
            };
            listIcons.Tag = parser;
            var dict = parser.ParseFolder("portrait");
            var dictBattle = parser.ParseFolder("portrait_battle", panelBattle.Size);
            var images = new List<Image>();
            var lists = new List<ListViewItem>();
            foreach (var kv in dict)
            {
                for (int i = 0; i < kv.Value.Length; i++)
                {
                    var index = images.Count;
                    lists.Add(new ListViewItem
                    {
                        Text = string.Format("{0} [{1}]", index, kv.Key),
                        ImageIndex = index,
                        Name = kv.Key,
                        Tag = new ID
                        {
                            Index = i,
                            Entries = kv.Value,
                            EntriesBattle = dictBattle[kv.Key]
                        }
                    });
                    images.Add(kv.Value[i].Image);
                }
            }

            this.BeginInvoke(new Action<Image[], ListViewItem[]>((imgs, lsts) =>
            {
                listIcons.Items.Clear();
                iconList.Images.Clear();
                iconList.Images.AddRange(imgs);
                listIcons.Items.AddRange(lsts);
                numericIndex.Maximum = lsts.Length - 1;
                panelContainer.Enabled = true;

                buttonSave.Enabled = true;
            }), images.ToArray(), lists.ToArray());
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var parser = (Parser)listIcons.Tag;
            string name = null;
            for (int i = 0; i < listIcons.Items.Count; i++)
            {
                var item = listIcons.Items[i];
                if (item.Name != name)
                {
                    name = item.Name;
                    var id = (ID)item.Tag;
                    parser.SaveFile(name, id.Entries, id.EntriesBattle);
                }
            }

            MessageBox.Show(this, "保存成功！", "保存", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

    class ID
    {
        public int Index { get; set; }
        public Entry[] Entries { get; set; }
        public Entry[] EntriesBattle { get; set; }
    }
}

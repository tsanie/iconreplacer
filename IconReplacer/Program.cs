﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IconReplacer
{
    class Entry
    {
        public byte[] Dummy;
        public byte Flags;
        public int Offset;
        public int Size;
        public int OriginalSize;
        public int Less;

        public byte[] Data;
        public Image Image;

        public int DataSize => Size - Less;
    }

    static class Program
    {
        public static readonly Yappy Yappy = new Yappy();

        private static readonly byte[] DCKey =
        {
            0x37, 0xea, 0x79, 0x85, 0x86, 0x29, 0xec, 0x94,
            0x85, 0x20, 0x7c, 0x1a, 0x62, 0xc3, 0x72, 0x4f,
            0x72, 0x75, 0x25, 0x0b, 0x99, 0x99, 0xbd, 0x7f,
            0x0b, 0x24, 0x9a, 0x8d, 0x85, 0x38, 0x0e, 0x39
        };
        private static readonly Aes AES = new AesManaged
        {
            Key = DCKey,
            Mode = CipherMode.ECB,
            Padding = PaddingMode.Zeros
        };

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }

        #region - Extension -
        public static int ReadInt(this Stream stream)
        {
            var buffer = new byte[4];
            stream.Read(buffer, 0, 4);
            return BitConverter.ToInt32(buffer, 0);
        }

        public static void WriteInt(this Stream stream, int value)
        {
            var buffer = BitConverter.GetBytes(value);
            stream.Write(buffer, 0, 4);
        }
        #endregion

        public static Entry[] ExtractFiles(string pckfile, Size size, out byte[] dummy)
        {
            using (var fsin = File.OpenRead(pckfile))
            {
                var buffer = new byte[8];
                fsin.Read(buffer, 0, 8);
                dummy = buffer;
                //fsin.Seek(8, SeekOrigin.Begin);

                var filecount = fsin.ReadInt();
                var entries = new Entry[filecount];
                for (var i = 0; i < filecount; i++)
                {
                    var entry = new Entry
                    {
                        Dummy = new byte[8]
                    };
                    fsin.Read(entry.Dummy, 0, 8);
                    entry.Flags = (byte)fsin.ReadByte();
                    entry.Offset = fsin.ReadInt();
                    entry.Size = fsin.ReadInt();
                    entry.OriginalSize = fsin.ReadInt();
                    entry.Less = fsin.ReadInt();

                    entries[i] = entry;
                }

                // extract
                for (var i = 0; i < filecount; i++)
                {
                    var entry = entries[i];
                    fsin.Seek(entry.Offset, SeekOrigin.Begin);
                    var data = new byte[entry.Size];
                    fsin.Read(data, 0, data.Length);
                    if ((entry.Flags & 2) == 2)
                    {
                        // decrypt
                        data = DecryptData(data);
                    }
                    if ((entry.Flags & 1) == 1)
                    {
                        // decompress
                        data = Yappy.Uncompress(data, entry.DataSize, entry.OriginalSize);
                    }
                    entry.Data = data;
                    try
                    {
                        entry.Image = Image.FromStream(new MemoryStream(data));
                        if (size != default(Size))
                        {
                            entry.Image = new Bitmap(entry.Image, size);
                        }
                    }
                    catch
                    {
                        entry.Image = null;
                    }
                }

                return entries;
            }
        }

        private static byte[] DecryptData(byte[] data)
        {
            using (var transform = AES.CreateDecryptor())
            {
                var result = transform.TransformFinalBlock(data, 0, data.Length);
                //AES.Clear();
                return result;
            }
        }

        private static byte[] EncryptData(byte[] data)
        {
            using (var transform = AES.CreateEncryptor())
            {
                var result = transform.TransformFinalBlock(data, 0, data.Length);
                AES.Clear();
                return result;
            }
        }
    }
}

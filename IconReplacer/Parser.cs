﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IconReplacer
{
    class Parser
    {
        public string Folder { get; set; }
        public Dictionary<string, byte[]> DummyDict { get; set; } = new Dictionary<string, byte[]>();

        public Dictionary<string, Entry[]> ParseFolder(string subfolder, Size size = default(Size))
        {
            if (!Directory.Exists(Folder) ||
                !Directory.Exists(Path.Combine(Folder, subfolder)))
            {
                return null;
            }
            var dict = new Dictionary<string, Entry[]>();

            foreach (var file in Directory.GetFiles(Path.Combine(Folder, subfolder), "c*.pck"))
            {
                var name = Path.GetFileName(file);
                byte[] dummy;
                dict.Add(name, Program.ExtractFiles(file, size, out dummy));
                DummyDict.Add(subfolder + name, dummy);
            }

            return dict;
        }

        public void SaveFile(string name, Entry[] entries, Entry[] entriesBattle)
        {
            var filename = name; // Path.GetFileNameWithoutExtension(name) + ".replace.pck";
            SaveFile(Path.Combine(Folder, "portrait", filename), DummyDict["portrait" + name], entries);
            SaveFile(Path.Combine(Folder, "portrait_battle", filename), DummyDict["portrait_battle" + name], entriesBattle);
        }

        public void SaveFile(string filename, byte[] dummy, Entry[] entries)
        {
            using (var fsout = File.Open(filename, FileMode.Create))
            {
                fsout.Write(dummy, 0, dummy.Length);
                fsout.WriteInt(entries.Length);

                int offset = 12 + entries.Length * 25;
                // write table-indexes
                foreach (var entry in entries)
                {
                    entry.Size = entry.Data.Length;
                    fsout.Write(entry.Dummy, 0, 8);
                    fsout.WriteByte(0);
                    fsout.WriteInt(offset);
                    fsout.WriteInt(entry.Size);
                    fsout.WriteInt(entry.Size);
                    fsout.WriteInt(0);

                    offset += entry.Size;
                }

                // write content
                const int DATA_SIZE = 4096;
                var data = new byte[DATA_SIZE];
                foreach (var entry in entries)
                {
                    fsout.Write(entry.Data, 0, entry.Size);
                }
            }
        }
    }
}

﻿namespace IconReplacer
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.iconList = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.textIconFolder = new System.Windows.Forms.TextBox();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.listIcons = new System.Windows.Forms.ListView();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemLarge = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemList = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemTile = new System.Windows.Forms.ToolStripMenuItem();
            this.panelRight = new System.Windows.Forms.Panel();
            this.buttonApply = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonReplace = new System.Windows.Forms.Button();
            this.numericIndex = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panelIcon = new System.Windows.Forms.Panel();
            this.panelBattle = new System.Windows.Forms.Panel();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.buttonSave = new System.Windows.Forms.Button();
            this.contextMenu.SuspendLayout();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericIndex)).BeginInit();
            this.panelContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // iconList
            // 
            this.iconList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.iconList.ImageSize = new System.Drawing.Size(128, 128);
            this.iconList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Icon文件夹：";
            // 
            // textIconFolder
            // 
            this.textIconFolder.AllowDrop = true;
            this.textIconFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textIconFolder.Location = new System.Drawing.Point(89, 7);
            this.textIconFolder.Name = "textIconFolder";
            this.textIconFolder.ReadOnly = true;
            this.textIconFolder.Size = new System.Drawing.Size(742, 21);
            this.textIconFolder.TabIndex = 1;
            this.textIconFolder.DragDrop += new System.Windows.Forms.DragEventHandler(this.textIconFolder_DragDrop);
            this.textIconFolder.DragEnter += new System.Windows.Forms.DragEventHandler(this.textIconFolder_DragEnter);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowse.Location = new System.Drawing.Point(837, 6);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(31, 23);
            this.buttonBrowse.TabIndex = 2;
            this.buttonBrowse.Text = "...";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // folderBrowser
            // 
            this.folderBrowser.Description = "Please select the \"asset/icon\" folder";
            this.folderBrowser.ShowNewFolderButton = false;
            // 
            // listIcons
            // 
            this.listIcons.AllowDrop = true;
            this.listIcons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listIcons.ContextMenuStrip = this.contextMenu;
            this.listIcons.HideSelection = false;
            this.listIcons.LargeImageList = this.iconList;
            this.listIcons.Location = new System.Drawing.Point(12, 34);
            this.listIcons.MultiSelect = false;
            this.listIcons.Name = "listIcons";
            this.listIcons.Size = new System.Drawing.Size(554, 479);
            this.listIcons.SmallImageList = this.iconList;
            this.listIcons.TabIndex = 3;
            this.listIcons.UseCompatibleStateImageBehavior = false;
            this.listIcons.SelectedIndexChanged += new System.EventHandler(this.listIcons_SelectedIndexChanged);
            this.listIcons.DragDrop += new System.Windows.Forms.DragEventHandler(this.textIconFolder_DragDrop);
            this.listIcons.DragEnter += new System.Windows.Forms.DragEventHandler(this.textIconFolder_DragEnter);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemLarge,
            this.toolStripMenuItemList,
            this.toolStripMenuItemTile});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(114, 70);
            // 
            // toolStripMenuItemLarge
            // 
            this.toolStripMenuItemLarge.Checked = true;
            this.toolStripMenuItemLarge.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItemLarge.Name = "toolStripMenuItemLarge";
            this.toolStripMenuItemLarge.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItemLarge.Text = "大图标";
            this.toolStripMenuItemLarge.Click += new System.EventHandler(this.toolStripMenuItemLarge_Click);
            // 
            // toolStripMenuItemList
            // 
            this.toolStripMenuItemList.Name = "toolStripMenuItemList";
            this.toolStripMenuItemList.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItemList.Text = "列表";
            this.toolStripMenuItemList.Click += new System.EventHandler(this.toolStripMenuItemList_Click);
            // 
            // toolStripMenuItemTile
            // 
            this.toolStripMenuItemTile.Name = "toolStripMenuItemTile";
            this.toolStripMenuItemTile.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItemTile.Text = "平铺";
            this.toolStripMenuItemTile.Click += new System.EventHandler(this.toolStripMenuItemTile_Click);
            // 
            // panelRight
            // 
            this.panelRight.AllowDrop = true;
            this.panelRight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelRight.Controls.Add(this.buttonApply);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.buttonReplace);
            this.panelRight.Controls.Add(this.numericIndex);
            this.panelRight.Controls.Add(this.label2);
            this.panelRight.Enabled = false;
            this.panelRight.Location = new System.Drawing.Point(706, 34);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(162, 479);
            this.panelRight.TabIndex = 4;
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(114, 56);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(48, 23);
            this.buttonApply.TabIndex = 6;
            this.buttonApply.Text = "替换";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "按照自定义图片：";
            // 
            // buttonReplace
            // 
            this.buttonReplace.Location = new System.Drawing.Point(114, 25);
            this.buttonReplace.Name = "buttonReplace";
            this.buttonReplace.Size = new System.Drawing.Size(48, 23);
            this.buttonReplace.TabIndex = 4;
            this.buttonReplace.Text = "替换";
            this.buttonReplace.UseVisualStyleBackColor = true;
            this.buttonReplace.Click += new System.EventHandler(this.buttonReplace_Click);
            // 
            // numericIndex
            // 
            this.numericIndex.Location = new System.Drawing.Point(3, 26);
            this.numericIndex.Name = "numericIndex";
            this.numericIndex.Size = new System.Drawing.Size(105, 21);
            this.numericIndex.TabIndex = 3;
            this.numericIndex.ValueChanged += new System.EventHandler(this.numericIndex_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "替换为索引：";
            // 
            // panelIcon
            // 
            this.panelIcon.AllowDrop = true;
            this.panelIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelIcon.Location = new System.Drawing.Point(572, 34);
            this.panelIcon.Name = "panelIcon";
            this.panelIcon.Size = new System.Drawing.Size(128, 128);
            this.panelIcon.TabIndex = 7;
            this.panelIcon.DragDrop += new System.Windows.Forms.DragEventHandler(this.panelIcon_DragDrop);
            this.panelIcon.DragEnter += new System.Windows.Forms.DragEventHandler(this.panelIcon_DragEnter);
            // 
            // panelBattle
            // 
            this.panelBattle.AllowDrop = true;
            this.panelBattle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBattle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBattle.Location = new System.Drawing.Point(572, 161);
            this.panelBattle.Name = "panelBattle";
            this.panelBattle.Size = new System.Drawing.Size(128, 352);
            this.panelBattle.TabIndex = 8;
            this.panelBattle.DragDrop += new System.Windows.Forms.DragEventHandler(this.panelBattle_DragDrop);
            this.panelBattle.DragEnter += new System.Windows.Forms.DragEventHandler(this.panelBattle_DragEnter);
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.buttonSave);
            this.panelContainer.Controls.Add(this.panelBattle);
            this.panelContainer.Controls.Add(this.label1);
            this.panelContainer.Controls.Add(this.panelRight);
            this.panelContainer.Controls.Add(this.textIconFolder);
            this.panelContainer.Controls.Add(this.panelIcon);
            this.panelContainer.Controls.Add(this.buttonBrowse);
            this.panelContainer.Controls.Add(this.listIcons);
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContainer.Location = new System.Drawing.Point(0, 0);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(880, 528);
            this.panelContainer.TabIndex = 9;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Enabled = false;
            this.buttonSave.Location = new System.Drawing.Point(793, 490);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "保存";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(880, 528);
            this.Controls.Add(this.panelContainer);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MinimumSize = new System.Drawing.Size(550, 567);
            this.Name = "FormMain";
            this.Text = "Icon Replacer";
            this.contextMenu.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericIndex)).EndInit();
            this.panelContainer.ResumeLayout(false);
            this.panelContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList iconList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textIconFolder;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.ListView listIcons;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericIndex;
        private System.Windows.Forms.Button buttonReplace;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelIcon;
        private System.Windows.Forms.Panel panelBattle;
        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLarge;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemList;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemTile;
        private System.Windows.Forms.Button buttonSave;
    }
}

